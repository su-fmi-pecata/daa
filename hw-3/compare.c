#include <stdio.h>

#define MAX_N 1024

int gl1;
int gl2;

void set(int* count, int* step, int n, int i, int newI, int stepVal) {
    if(newI <= n && (count[newI] == 0 || count[newI] - count[i] > 1)) {
        count[newI] = 1 + count[i];
        step[newI] = stepVal;
    }
}

void calc(int n) {
    int countStep[MAX_N] = { 0 };
    int whichStep[MAX_N] = { 0 };

    for(int i = 1;  i < n;  i++) {
        if(i != 1 && countStep[i] == 0) continue;

        set(countStep, whichStep, n, i, i*3, 3);
        set(countStep, whichStep, n, i, i*2, 2);
        set(countStep, whichStep, n, i, i+1, 1);
    }

    int c = 0;
    while(n > 1) {
        gl1++;
        countStep[c++] = whichStep[n];
        if(whichStep[n] == 1) {
            n--;
        } else {
            n /= whichStep[n];
        }
    }
    for(int i = c-1;  i >= 0;  i--) {
        printf("%d", countStep[i]);
    }
}



void ALG(unsigned int n) {
    gl2++;
    if(n % 3 == 0) {
        ALG(n / 3);
        printf("3");
    } else if(n % 2 == 0) {
        ALG(n / 2);
        printf("2");
    } else if (n > 1) {
        ALG(n - 1);
        printf("1");
    } else {
        gl2--;
    }
}

int main() {
    int n;
    while(scanf("%d", &n) && n != 0) {
        gl1 = 0;
        gl2 = 0;
        printf("%d\t", n);
        if (n > 1) {
            calc(n);
            printf("\t->\t");
            ALG(n);
            printf("\n%d\t%d\n-----------------\n", gl1, gl2);
            if(gl1 != gl2) {
                fprintf(stderr, "n: %d\t%d\t%d\t%d\n", n, (gl1-gl2), gl1, gl2);
            }
        }
        printf("\n");
    }

    return 0;
}