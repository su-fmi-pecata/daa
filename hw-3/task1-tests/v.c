#include <stdio.h>

int ans[(1<<20)];

int min( int a, int b ) {
    if ( a <= b ) { return a; }
    return b;
}

int compute(int n) {
    if ( ans[n] ) { return ans[n]; }
    if ( n == 1 ) { return 0; }
    compute(n-1);
    ans[n] = ans[n-1]+1;
    if ( n%3 == 0 ) {
        compute(n/3);
        ans[n] = min(ans[n], ans[n/3]+1);
    }
    if ( n%2 == 0 ) {
        compute(n/2);
        ans[n] = min(ans[n], ans[n/2]+1);
    }
    return ans[n];
}

void solve ( int n ){
    compute(n);
    int len = ans[n];
    char result[len+1];
    result[len] = '\0';

    for ( int i = 0; n != 1 ; i ++ ) {
        if ( n%3 == 0 && ans[n] == ans[n/3]+1 ) {
            result[i] = '3';
            n /= 3;
        }else if ( n%2 == 0 && ans[n] == ans[n/2]+1 ) {
            result[i] = '2';
            n /= 2;
        }else {
            result[i] = '1';
            n --;
        }
    }
    for ( int i = len-1 ; i >= 0 ; i -- ) {
        printf("%c", result[i]);
    }
    printf("\n");
}

int main () {
    int n;
    for ( scanf("%d", &n) ; n ; scanf("%d", &n) ) {
        solve(n);
    }
    return 0;
}
