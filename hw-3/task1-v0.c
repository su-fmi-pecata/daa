#include <stdio.h>

#define MAX_N 1024

void set(int* count, int* step, int n, int i, int newI, int stepVal) {
  if (newI <= n && (count[newI] == 0 || count[newI] - count[i] > 1)) {
    count[newI] = 1 + count[i];
    step[newI] = stepVal;
  }
}

void calc(int n) {
  int countStep[MAX_N] = {0};
  int whichStep[MAX_N] = {0};

  for (int i = 1; i < n; i++) {
    if (i != 1 && countStep[i] == 0) continue;

    set(countStep, whichStep, n, i, i * 3, 3);
    set(countStep, whichStep, n, i, i * 2, 2);
    set(countStep, whichStep, n, i, i + 1, 1);
  }

  int c = 0;
  while (n > 1) {
    countStep[c++] = whichStep[n];
    if (whichStep[n] == 1) {
      n--;
    } else {
      n /= whichStep[n];
    }
  }
  for (int i = c - 1; i >= 0; i--) {
    printf("%d", countStep[i]);
  }
}

int main() {
  int n;
  while (scanf("%d", &n) && n != 0) {
    if (n > 1) calc(n);
    printf("\n");
  }

  return 0;
}