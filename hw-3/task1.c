#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VERBOSE 0
#define LOGGER_HELPER(level, f_, ...) \
  if (VERBOSE >= level) fprintf(stderr, (f_), ##__VA_ARGS__);
#define LOGGER(f_, ...) LOGGER_HELPER(1, (f_), ##__VA_ARGS__);
#define TRACE(f_, ...) LOGGER_HELPER(2, (f_), ##__VA_ARGS__);

int hasBeenExtended = 0;
int hasSkipped = 0;
int firstSkipped = 1;
int maxSize = 1024;
int maxReadN = 1;
int* countStep;
int* whichStep;

int* allocateArray(int len) { return (int*)calloc(len, sizeof(int)); }

void set(int* count, int* step, int n, int i, int newI, int stepVal) {
  TRACE("set: i:%d     ni:%*d  count:%*d step:%d skip:%d", i, 5, newI, 2,
              count[i], stepVal, hasSkipped);

  if (newI <= maxSize) {
    TRACE("\tc[%5d]=%2d c[%5d]=%d", i, count[i], newI, count[newI]);

    if (count[newI] == 0 || count[newI] - count[i] > 1) {
      TRACE("\t->\t");

      count[newI] = 1 + count[i];
      step[newI] = stepVal;

      TRACE("c[%5d]=%2d s[%5d]=%d", newI, count[newI], newI, stepVal);
    }
  } else {
    if (hasSkipped == 0) {
      TRACE("\t\t[s: %d]", i);
      firstSkipped = i;
    }

    hasSkipped = 1;
  }

  TRACE("\n");
}

int upperPowerOfTwo(int v) {
  v--;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v++;
  return v;
}

void extend(int n) {
  int newMax = upperPowerOfTwo(n + 1);
  LOGGER("Extending from %d to %d\n", maxSize, newMax);

  int* tmp;

  // copy countStep
  tmp = allocateArray(newMax);
  memcpy(tmp, countStep, maxSize * sizeof(int));
  free(countStep);
  countStep = tmp;
  TRACE("Copied countStep\n");

  // copy whichStep
  tmp = allocateArray(newMax);
  memcpy(tmp, whichStep, maxSize * sizeof(int));
  free(whichStep);
  whichStep = tmp;
  TRACE("Copied whichStep\n");

  // finishing
  maxSize = newMax;
  hasSkipped = 0;
  hasBeenExtended = 1;
  TRACE("Update variables\n");
  LOGGER("Successfully expanded\n");
}

void print(int n) {
  if (n <= 1) {
    return;
  } else {
    switch (whichStep[n]) {
      case 1:
        print(n - 1);
        break;
      case 2:
        print(n / 2);
        break;
      case 3:
        print(n / 3);
        break;
    }
    printf("%d", whichStep[n]);
  }
}

void calc(int n) {
  TRACE("\n\nnew calc: n:%d      maxN:%d\n", n, maxReadN);
  for (int i = hasBeenExtended ? firstSkipped : maxReadN; i < n; i++) {
    TRACE("calc: ext:%d    skip:%d    n:%*d    i:%*d\n", hasBeenExtended,
                hasSkipped, 7 - 7, n, 10 - 10, i);

    if (i != 1 && countStep[i] == 0) {
      TRACE("+++++++++++++++++\nOOOOOPS %d\n+++++++++++++++++=\n\n", i);
      continue;
    }

    set(countStep, whichStep, n, i, i * 3, 3);
    set(countStep, whichStep, n, i, i * 2, 2);
    set(countStep, whichStep, n, i, i + 1, 1);
  }

  if (hasBeenExtended == 1 || maxReadN < n) maxReadN = n;
  if (hasSkipped == 0) firstSkipped = n;
  hasBeenExtended = 0;
}

void findPath(int n) {
  if (n >= maxSize) extend(n);
  if (n > maxReadN) calc(n);
  print(n);
}

int main() {
  countStep = allocateArray(maxSize);
  whichStep = allocateArray(maxSize);
  int n;
  while (scanf("%d", &n) && n != 0) {
    if (n > 1) findPath(n);
    printf("\n");
  }

  return 0;
}
