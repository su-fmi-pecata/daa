const int n = 7;
typedef bool Graph[n][n];

int degree(int vertex, Graph G) {
  int dgr = 0;
  for(int i = 0;  i < n;  i++)
    dgr += G[vertex][i];
  return dgr;
}

int chooseFirst() {
  return 4; // Chosen by fair dice roll.
            // Guranteed to be random
}

bool scorpioSpecialCaseBody(int body, Graph G) {
  int string = -1;
  for(int i = 0;  i < n;  i++) 
    if(!G[body][i] && body != i)
      string = i;

  
  int tail = -1;
  int stringDegree = 0;
  for(int i = 0;  i < n;  i++) {
    if(G[string][i]) {
      tail = i;
      stringDegree++;
    }
  }
  
  if(stringDegree != 1) return false;
  return degree(tail, G) == 2;
}

bool scorpioSpecialCaseTail(int tail, Graph G) {
  int conn[2] = { 0 };
  
  int tailDegree = 0;
  for(int i = 0;  i < n;  i++) {
    if(G[tail][i]) {
      conn[tailDegree++] = degree(i, G);
    }
  }

  return (conn[0] == 1 && conn[1] == n - 2)
      || (conn[0] == n - 2 && conn[1] == 1);
}

bool scorpioSpecialCaseString(int string, Graph G) {
  int tail = -1;
  for(int i = 0;  i < n;  i++) {
    if(G[string][i]) {
      tail = i;
      break;
    }
  }

  int tailDegree = 0;
  int body = -1;
  for(int i = 0;  i < n;  i++) {
    if(G[tail][i]) {
      tailDegree++;
      if(i != string) body = i;
    }
  }

  if(tailDegree != 2) return false;

  return degree(body, G) == n - 2;
}

bool scorpioNormalCase(int firstVertex, Graph G) {
  int B[n] = { 0 };
  int S[n] = { 0 };
  int bLen = 0;
  int sLen = 0;

  // Setup sets
  for(int i = 0;  i < n;  i++) {
    if(G[firstVertex][i]) {
      B[bLen++] = i;
    } else if(i != firstVertex) {
      S[sLen++] = i;
    }
  }

  // start removing vertices to determine is scorpio
  int sCurr = 0;
  int bCurr = 0;
  while(sCurr < sLen && bCurr < bLen) {
    if(G[S[sCurr]][B[bCurr]]) {
      sCurr++;
    } else {
      bCurr++;
    }
  }

  // B must be empty and S[sCurr] must be string
  if (bCurr != bLen || degree(S[sCurr], G) != 1) return false;
  return scorpioSpecialCaseString(S[sCurr], G);
       
}

bool IsScorpion(Graph G) {
  if(n < 5) return false; // must have atleast 5 vertices

  int first = chooseFirst();
  int degreeFirst = degree(first, G);
  if (degreeFirst == 0 || degreeFirst == n - 1) return false;

  switch(degreeFirst) {
    case 1:
      return scorpioSpecialCaseString(first, G);
    case 2:
      return scorpioSpecialCaseTail(first, G);
    case n - 2:
      return scorpioSpecialCaseBody(first, G);
    default:
      return scorpioNormalCase(first, G);
  }
}